## 服务器推荐配置
| 名称        | CPU    |  内存  |  硬盘  |  价格  |
| --------    | -----:   | :----: | :----: | :----: |
| 服务器(标准型S2机型 5M 双机)       | 2核      |   8G    |   50G    |   2970元/三年    |
| MySQL高可用版| 1核      |   1G    |   100G    | 423元/三年    |

| COS资源包     | 大小     |  价格     |
| --------     | -----:   | :----:   |
| 标准型存储容量 |200GB	  |1年	171元|
| 下行流量      | 500GB	  |3个月	177元|

[抢购地址：https://cloud.tencent.com](https://cloud.tencent.com/act/cps/redirect?redirect=1044&cps_key=30280f92fc381dfc9e1d9e0e23d25a18&from=console)

# 使用须知
## ✅允许
- 个人学习使用
- 允许用于学习、毕设等
- 允许进行商业使用，但是要保留 footer 水印，请自觉遵守使用协议，别给公司带来不必要麻烦，如需要商业使用推荐购买商业版

### 微同商城商业版
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/grocery/20181228/1114545734c867.jpg "微同商城商业版")

[商业版与开源版差异](http://fly2you.cn/business/index)

## ❌禁止
- 将本项目的代码和资源进行任何形式的出售
- 利用本项目的代码和资源进行任何商业行为
- 擅自窃用，即属严重侵权行为，与盗窃无异。产生的一切任何后果责任由侵权者自负

## 🙏呼吁
- 维护国内开源环境，人人有责！

# 微信小程序商城（Java版）

## 获得荣誉
### GVP
![](
https://platform-wxmall.oss-cn-beijing.aliyuncs.com/GVP.jpg "GVP")

## 官方首页
* [演示地址](http://fly2you.cn)
* [最新开发文档](http://fly2you.cn/guide/index)

## 新手必看启动教程
- [https://www.bilibili.com/video/av66149752](https://www.bilibili.com/video/av66149752)

### 微同商城开源版体验：
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/gh_a7a467438863_344.jpg "微同商城开源版")

* 官方QQ群：<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=2d02d83d8be4c2cb6848bbae1df1037ba2acddecd2a1aa8cef7b3e4ab4ff75aa"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="platform-wechat-mall ①群" title="platform-wechat-mall ①群"></a><a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=990a15d445ef791dba99d22d9772c06ac7894ffa6ac639b1eec530554c432583"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="platform-wechat-mall ②群" title="platform-wechat-mall ②群"></a>
* git：[https://gitee.com/fuyang_lipengjun/platform](https://gitee.com/fuyang_lipengjun/platform)
* 代码生成工具IDEA插件
    * git：[https://gitee.com/fuyang_lipengjun/platform-gen](https://gitee.com/fuyang_lipengjun/platform-gen)
    
# 注意
Entity里不是缺少get、set方法，Eclipse、IDEA请先安装lombok插件
 
## 技术选型
* 1 后端使用技术
    * 1.1 springframework4.3.7.RELEASE
    * 1.2 mybatis3.1.0、MyBatis-Plus 3.1.0
    * 1.3 shiro1.3.2
    * 1.4 servlet3.1.0
    * 1.5 druid1.0.28
    * 1.6 slf4j1.7.19
    * 1.7 fastjson1.2.30
    * 1.8 poi3.15
    * 1.9 velocity1.7
    * 1.10 quartz2.2.3
    * 1.11 mysql5.1.39
    * 1.12 swagger2.4
    * 1.13 j2cache2.3.22-release
    * 1.14 weixin-java-mp3.2.0
    * 1.15 MybatisPlus3.1.0
    * 1.16 lombok
        
* 2 前端使用技术
    * 2.1 Vue2.5.1
    * 2.2 iview
    * 2.3 layer3.0.3
    * 2.4 jquery2.2.4
    * 2.5 bootstrap3.3.7
    * 2.6 jqgrid5.1.1
    * 2.7 ztree3.5.26
    * 2.8 froala_editor1.2.2

## 项目结构
~~~
platform-wechat-mall
|--platform-admin 后台管理
|--platform-api 微信小程序商城api接口
|--platform-common 公共模块
|--platform-framework 系统WEB合并，请打包发布此项目
|--platform-gen 代码生成
|--platform-mp 微信公众号模块
|--platform-schedule 定时任务
|--platform-shop 商城后台管理
|--uni-color-ui uni-mall商城参考项目
|--uni-mall 移动端商城
|--wx-mall 微信小程序商城
~~~

## 实现功能

* 一：会员管理
    * a 会员管理
    * b 会员等级
    * c 收货地址管理
    * d 会员优惠劵
    * e 会员收藏
    * f 会员足迹
    * g 搜索历史
    * h 购物车

* 二：商城配置
    * a 区域配置
    * b 商品属性种类
    * c 品牌制造商
    * d 商品规格
    * e 订单管理
    * f 商品类型
    * g 渠道管理
    * h 商品问答
    * i 反馈
    * j 关键词

* 三：商品编辑
    * a 所有商品
    * b 用户评论
    * c 产品设置
    * d 商品规格
    * e 商品回收站

* 四：推广管理
    * a 广告列表
    * b 广告位置
    * c 优惠劵管理
    * d 专题管理
    * e 专题分类

* 五：订单管理
    * a 所有订单管理

* 六：系统管理
    * a 管理员列表
    * b 角色管理
    * c 菜单管理
    * d SQL监控
    * e 定时任务
    * f 参数管理
    * g 代码生成器
    * h 系统日志
    * i 文件上传
    * j 通用字典表
        
* 六：短信服务平台  
    * **需要短信验证码、短信通知、短信营销的客户进群私聊我**
    * a 配置短信平台账户信息
    * b 向外提供发送短信接口：
    ```
    http://域名:端口/api/sendSms?mobile=13000000000,15209831990&content=发送的短信内容  
    安全起见，需配置有效IP地址。platform.properties -> sms.validIp
    ```

## 安装教程

* 配置环境（推荐jdk1.8、maven3.3、tomcat8、mysql5.7、redis4.0.1）
* 创建数据库
* 依次初始化sql脚本 
    * /_sql/platform.sql
    * /_sql/sys_region.sql
* 导入项目到IDE中
* 导入支付证书至/platform-shop/src/main/resources/cert/目录下（申请商户号、开通微信支付、下载支付证书）
* 修改配置文件 /platform-admin/src/main/resources/dev/platform.properties
    * jdbc.url
    * jdbc.username
    * jdbc.password
    * wx.appId
    * wx.secret
    * wx.mchId
    * wx.paySignKey
    * wx.notifyUrl
    * sms.validIp
    * mp.appId
    * mp.secret
    * mp.token
    * mp.aesKey
* 修改配置文件 /platform-admin/src/main/resources/j2cache.properties
    * redis.hosts
    * redis.password
* 启动后台项目（参照<a href="#doc">开发文档</a>）
* 打开微信开发者工具
* 导入 /wx-mall填写appId
* 修改 /wx-mall/config/api.js里API_BASE_URL的值
* 启动方法参考视频
* 使用eclipse启动项目后默认访问路径
    * [http://localhost:8080/platform-framework](http://localhost:8080/platform-framework)
* 使用idea启动项目后默认访问路径
    * [http://localhost:8080](http://localhost:8080)
    
* 微信支付需要如下步骤
*    1：配置参数  
*        1.1 wx.appId
*        1.2 wx.mchId：商家账号，在微信支付平台-商户信息-微信支付商户号
*        1.3 wx.notifyUrl:回调地址
*        1.4 wx.tradeType:交易类型APP，约定填写JSAPI。请见文档：https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_sl_api.php?chapter=7_11&index=2
*        1.5 wx.paySignKey:数字签证。在支付平台->账户中心->API安全->API秘钥
*    2：微信支付平台  
*        2.1  申请支付，https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_sl_api.php?chapter=3_1
*            2.1.1      到https://mp.weixin.qq.com点击微信支付，申请接入。
*                2.1.1.1 已有微信支付账号的参考下列指引。 https://pay.weixin.qq.com/static/pay_setting/appid_protocol.shtml
*            2.1.2      登录微信商户平台，登录商户平台-产品中心-账号关联（AppID绑定），进入授权申请页面；
*                2.1.2.1    商户平台https://pay.weixin.qq.com/index.php    
*        2.2 https://pay.weixin.qq.com/index.php/core/home/login
*        2.3 小程序支付开发文档，https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_sl_api.php?chapter=7_10&index=1
*    3：小程序开发需要进行如下配置  
*        3.1 参考文档，https://developers.weixin.qq.com/miniprogram/dev/framework/server-ability/message-push.html
    

* 小程序官方开发文档，https://developers.weixin.qq.com/miniprogram/dev/framework/
* 小程序注册页，https://mp.weixin.qq.com/wxopen/waregister?action=step1
* 小程序后台入口，https://mp.weixin.qq.com/。可以在菜单 “设置”-“开发设置” 看到小程序的 AppID 。使用小程序的账号【daxiangpian02@163.com】，不要使用公众号的账号。
* 小程序的配置，https://developers.weixin.qq.com/miniprogram/dev/framework/config.html
* 小程序获取用户信息需要在platform.properties中配置wx.appId和wx.secret
* 小程序支付,https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_3&index=1

* 小程序在微信开发工具上点击上传成功之后，登录小程序管理后台 - 开发管理 - 开发版本 就可以找到刚提交上传的版本了。可以将这个版本设置 体验版 或者是 提交审核
* 小程序如何进行提交审核
*    在开发者工具中上传了小程序代码之后，登录 小程序管理后台 - 开发管理 - 开发版本 找到提交上传的版本。在开发版本的列表中，点击 提交审核 按照页面提示，填写相关的信息，即可以将小程序提交审核。
*  WeUI[https://github.com/Tencent/weui-wxss]
* 开发环境，绑定手机暂时关闭发短信功能。后台会打印模拟发短信的日志。
*  微信商户服务平台，https://pay.weixin.qq.com/index.php/partner/public/home
微信支付，退款流程参考下列链接。并注意需要安装证书。【https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_4】

*Idea专业版下载及破解。https://blog.csdn.net/tcanhe/article/details/95308949

* 遗留问题：
*    1：我的订单，没有数据时是个白板，建议加点文字进行提示更有好些。参考"我的优惠券"
*    2：我的收藏，没有数据时是个白板，建议加点文字进行提示更有好些。参考"我的优惠券"
*    3：我的足迹，没有数据时是个白板，建议加点文字进行提示更有好些。参考"我的优惠券"
*    4：联系客服，开发者工具不提供该功能，待确认是否可以正常使用。
*    5：帮助中心，没有数据时是个白板，建议加点文字进行提示更有好些。参考"我的优惠券"
*    6：意见反馈有下列问题
*        6.1：输入框输入文字会自动左缩进
*        6.2：100%时提交按钮无法正常显示
*        6.3：保存时报错
*    7：建议"我的订单","我的收藏","我的足迹","帮助中心"等增加一个头部，标示当前页面位置。参考帮助中心。
*    8：绑定手机功能有下列问题
*        8.1：发短信验证码发送成功后前端总是保持加载状态，需要在成功或是失败是都去除加载状态，错误出现提示。
*        8.2：提交失败后，重新输入正确的，再点击提交，没有反应。
*    9：地址管理有下列问题
*        9.1：可以设置多个默认地址。期望只设置一个默认地址就可以

*    10：订单管理
*        10.1：没有数据时是个大白页，建议加点文字进行提示更有好些。参考"我的优惠券"
*        10.2：已经取消的订单，在订单详情页还是会出现取消按钮。建议隐藏。
*        10.3：商品买了3个，编辑减少为1件时，保存后，回到列表页商品又恢复到3件。【bug】
*        10.4：支付订单页面建议增加一个返回按钮，保障支付失败可以返回使用。
*        10.5：我的订单->（未支付的订单）去支付->调起二维码进行支付->取消支付->关闭二维码->（未支付的订单）去支付->报该订单已经支付。【bug】
*        10.6：调起二维码进行支付，但是又没有支付，然后再次调起进行支付时。微信端报支付失败,201 商户订单号重复。这时怎么解决？【提示】
*        10.7：只有订单号和订单状态。期望可以加上商品名称。
*        10.8：已支付但未发货的订单允许取消。这是对的。但是取消后的费用怎么返回？
*        10.9：已发货的订单不允许取消。这是对的。
*        10.9：点击完已收货后回到列表页，还是显示订单为待收货状态。期望显示已完成。
*        10.10：已完成的订单页面建议隐藏取消按钮。
*    11：商品详情页
*        11.1：未选择商品型号等时点击加入购物车没有任何动作，希望可以直接可以跳到型号页面。【bug】
*        11.2：选择完商品，添加到购物车。此时点击购物车图标，期望跳到购物车页面。结果是什么都做也没有。【bug】
*        11.3：选择规格经常不跳转。
*    12：购买流程
*        12.1：选择0.01元的商品，到购物车里确显示459元。例如商品锅具-测试商品1.
*        12.2：取消订单失败前端没有响应。建议增加提示操作失败。
*        12.2：取消订单，退款成功。提示语只显示了“提示”
微信支付绑定问题
    公众号(包括小程序)请前往公众平台（mp.weixin.qq.com）>微信支付>M-A授权页面确认







在开发版本的列表中，点击 提交审核 按照页面提示，填写相关的信息，即可以将小程序提交审核。

## 生产环境打包
    platform-wechat-mall>mvn package -P prod

***
### 关注微信公众号，第一时间获取项目最新动向，即将推出视频教程
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180708/qr.jpg "微信公众号")

## 页面展示
### 登录页面
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180708/login.png "登录")
### 首页
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180708/index.png "首页")
### 发送短信
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/3.png "发送短信")
### 捐赠
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/4.png "捐赠")
### 小程序首页
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/5.png "小程序首页")
### 专题
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/6.png "专题")
### 分类
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/7.png "分类")
### 购物车
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/8.png "购物车")
### 登录授权
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/9.png "登录授权")
### 优惠券
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/10.png "优惠券")
### 小程序并联手机
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/11.png "并联手机")
### VUE页面
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/12.png "VUE页面")

***

### <a name="doc">开发文档目录</a>
![](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/catalog.png "开发文档目录")



### ### ### ### ### ### ### ### ### ### ### ### ### ### 
nideshop_goods :商品表
nideshop_category：商品分类表
nideshop_attribute_category：
nideshop_brand：品牌表
nideshop_goods_specification: 
nideshop_specification：
nideshop_product:产品表

nideshop_comment：评论表

nideshop_user:商城用户表
