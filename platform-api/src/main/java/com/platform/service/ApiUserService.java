package com.platform.service;

import com.platform.dao.ApiUserLevelMapper;
import com.platform.dao.ApiUserMapper;
import com.platform.entity.SmsLogVo;
import com.platform.entity.UserLevelVo;
import com.platform.entity.UserVo;
import com.platform.utils.RRException;
import com.platform.validator.Assert;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
public class ApiUserService {
    @Autowired
    private ApiUserMapper apiUserMapper;
    @Autowired
    private ApiUserLevelMapper userLevelDao;

    public UserVo queryObject(Long userId) {
        return apiUserMapper.queryObject(userId);
    }

    public UserVo queryByOpenId(String openId) {
        return apiUserMapper.queryByOpenId(openId);
    }

    public List<UserVo> queryList(Map<String, Object> map) {
        return apiUserMapper.queryList(map);
    }

    public int queryTotal(Map<String, Object> map) {
        return apiUserMapper.queryTotal(map);
    }

    public void save(String mobile, String password) {
        UserVo user = new UserVo();
        user.setMobile(mobile);
        user.setUsername(mobile);
        user.setPassword(DigestUtils.sha256Hex(password));
        user.setRegister_time(new Date());
        apiUserMapper.save(user);
    }

    public void save(UserVo userVo) {
        apiUserMapper.save(userVo);
    }

    public void update(UserVo user) {
        apiUserMapper.update(user);
    }

    public void delete(Long userId) {
        apiUserMapper.delete(userId);
    }

    public void deleteBatch(Long[] userIds) {
        apiUserMapper.deleteBatch(userIds);
    }

    public UserVo queryByMobile(String mobile) {
        return apiUserMapper.queryByMobile(mobile);
    }

    public long login(String mobile, String password) {
        UserVo user = queryByMobile(mobile);
        Assert.isNull(user, "手机号或密码错误");

        //密码错误
        if (!user.getPassword().equals(DigestUtils.sha256Hex(password))) {
            throw new RRException("手机号或密码错误");
        }

        return user.getUserId();
    }

    public SmsLogVo querySmsCodeByUserId(Long user_id) {
        return apiUserMapper.querySmsCodeByUserId(user_id);
    }
    public SmsLogVo querySmsCodeByPhone(String phone) {
        return apiUserMapper.querySmsCodeByPhone(phone);
    }


    public int saveSmsCodeLog(SmsLogVo smsLogVo) {
        return apiUserMapper.saveSmsCodeLog(smsLogVo);
    }

    public String getUserLevel(UserVo loginUser) {
        String result = "普通用户";
        UserLevelVo userLevelVo = userLevelDao.queryObject(loginUser.getUser_level_id());
        if (null != userLevelVo) {
            result = userLevelVo.getName();
        }
        return result;
    }
}
