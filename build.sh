#!/usr/bin/env bash
# 拉取最新的源码
git pull

# 停止已运行的服务
sh run.sh stop



# 执行打包
mvn clean package -Pprod -Dmaven.test.skip=true

rm /opt/apache-tomcat-8-9898-shop/webapps/platform-framework.war
echo "------------------------------------------------------------"
echo "删除/opt/apache-tomcat-8-9898-shop/webapps/platform-framework.war"
rm /opt/apache-tomcat-8-9898-shop/webapps/platform-framework.war
echo "删除/opt/apache-tomcat-8-9898-shop/webapps/ROOT.war"

rm -rf /opt/apache-tomcat-8-9898-shop/webapps/platform-framework
echo "删除文件夹/opt/apache-tomcat-8-9898-shop/webapps/platform-framework"

rm -rf /opt/apache-tomcat-8-9898-shop/webapps/ROOT
echo "删除文件夹/opt/apache-tomcat-8-9898-shop/webapps/ROOT"

mv /opt/projects/platform/platform-framework/target/platform-framework.war /opt/apache-tomcat-8-9898-shop/webapps/ROOT.war
echo "复制/opt/projects/platform/platform-framework/target/platform-framework.war到/opt/apache-tomcat-8-9898-shop/webapps/ROOT.war"
echo "------------------------------------------------------------"
# 运行
sh run.sh start
echo "platform-framework 打包完毕,并使用了sh run.sh start进行启动"
# /usr/local/nginx/sbin/nginx  -s reload
# /usr/local/nginx/sbin/nginx  -s reload