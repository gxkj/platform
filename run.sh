#!/bin/sh
# chkconfig: 345 99 10
# description: Auto-starts tomcat7
# /etc/init.d/tomcat
# Tomcat auto-start
RETVAL=0
export LANG=zh_CN.UTF-8
#export JAVA_HOME=/usr/java/jdk1.8.0_211-amd64
#export JRE_HOME=/usr/java/jdk1.8.0_211-amd64/jre
export MY_CATALINA_HOME=/opt/apache-tomcat-8-9898-shop

is_exist(){
	pid=`ps -ef|grep $MY_CATALINA_HOME|grep -v grep|awk '{print $2}' `
	if [ -z "${pid}" ]; then
		return 1
	else
		return 0
	fi
}

start()
{
is_exist
if [ $? -eq "0" ]; then

    echo "$MY_CATALINA_HOME running. pid=${pid}"
else
    echo $"Starting $MY_CATALINA_HOME"

    nohup  $MY_CATALINA_HOME/bin/startup.sh &
    sleep 1
    status
    RETVAL=$?
    echo " OK"
    return $RETVAL

  fi
}
stop()
{
    is_exist
	if [ $? -eq "0" ]; then
		kill -9 $pid
		echo "${pid} stopped,$MY_CATALINA_HOME"
		 return $RETVAL
	else
		echo "$MY_CATALINA_HOME not running"
	fi

}

status()
{
  ps -ef|grep $MY_CATALINA_HOME
}

case "$1" in
 start)
   start
   ;;
 stop)
   stop
   ;;

 restart)
   echo $"Restaring $MY_CATALINA_HOME"
   $0 stop
   sleep 1
   $0 start
   ;;
   status)
   status
   ;;
 *)
   echo $"Usage: $0 {start|stop|restart|status}"
   exit 1
   ;;
esac
exit $RETVAL